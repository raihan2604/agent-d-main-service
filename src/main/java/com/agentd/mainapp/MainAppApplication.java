package com.agentd.mainapp;

import com.agentd.mainapp.core.UserAgentD;
import com.agentd.mainapp.handler.MessageHandler;
import com.agentd.mainapp.repository.UserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

@SpringBootApplication
@LineMessageHandler
public class MainAppApplication extends SpringBootServletInitializer {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    private UserRepository repo = new UserRepository();
    static LogManager lgmngr = LogManager.getLogManager();
    static Logger log = lgmngr.getLogger(Logger.GLOBAL_LOGGER_NAME);
    MessageHandler handler = new MessageHandler();


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MainAppApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(MainAppApplication.class, args);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
        String pesan = messageEvent.getMessage().getText();
        String[] pesanSplit = pesan.split("/");
        String userId = messageEvent.getSource().getSenderId();
        if(repo.getUserById(userId)==null){
            String url = "https://agent-d-tugas.herokuapp.com/tugas/register-user/"+userId;
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity result = restTemplate.postForObject(url,(Object)"", ResponseEntity.class);
            repo.addUser(new UserAgentD(userId));
        }
        UserAgentD user = repo.getUserById(userId);
        String jawaban = handler.periksaMessage(pesanSplit,user);

        String replyToken = messageEvent.getReplyToken();
        replyText(replyToken, jawaban);
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent event) {
        UserAgentD user = new UserAgentD(event.getSource().getSenderId());
        repo.addUser(user);
    }



    private void replyText(String replyToken, String jawaban){
        TextMessage jawabanDalamBentukTextMessage = new TextMessage(jawaban);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawabanDalamBentukTextMessage))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            log.log(Level.INFO, "Error while sending message");
            Thread.currentThread().interrupt();
        }
    }

}
