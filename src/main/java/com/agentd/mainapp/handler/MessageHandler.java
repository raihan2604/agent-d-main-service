package com.agentd.mainapp.handler;

import com.agentd.mainapp.core.UserAgentD;

public class MessageHandler {

    String tidakDikenal = "Command tidak dikenal";
    TugasHandler handlerTugas;

    public MessageHandler(){
        handlerTugas = new TugasHandler();
    }

    public String periksaMessage(String[] pesanSplit, UserAgentD user){
        String jawaban = "";
        switch (pesanSplit[0]){
            case("tambah"):
                switch (pesanSplit[1]){
                    case("tugas individu"):
                        jawaban = handlerTugas.tambahTugasIndividu(pesanSplit[2], pesanSplit[3], pesanSplit[4], user);
                        break;
                    default:
                        jawaban = tidakDikenal;
                }
                break;
            case("lihat"):
                switch (pesanSplit[1]){
                    case ("tugas individu"):
                        jawaban = handlerTugas.lihatTugasIndividu(user);
                        break;
                    default:
                        jawaban = tidakDikenal;
                }
                break;
            default:
                jawaban = tidakDikenal;
        }

        return jawaban;
    }
}
