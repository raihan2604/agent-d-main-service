package com.agentd.mainapp.handler;

import com.agentd.mainapp.core.UserAgentD;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;

public class TugasHandler {

    public String tambahTugasIndividu(String nama, String deskripsi, String deadline, UserAgentD user) {
        String url = "https://agent-d-tugas.herokuapp.com/tugas/register-tugas/" + user.getId() + "/" + nama + "/" + deskripsi + "/" + deadline;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity result = restTemplate.postForObject(url, (Object) "", ResponseEntity.class);
        return nama + " berhasil ditambahkan sebagai tugas individu";
    }

    public String lihatTugasIndividu(UserAgentD user) {
        String jawaban = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String url = "https://agent-d-tugas.herokuapp.com/tugas/lihat-tugas/" + user.getId();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> result = restTemplate.getForEntity(url, List.class);
        List<LinkedHashMap<String, String>> list = result.getBody();
        for (int i = 0; i < list.size(); i++) {
            jawaban += "nama tugas : " + list.get(i).get("name") + "\n";
            jawaban += "deskripsi : " + list.get(i).get("desc") + "\n";
            jawaban += "deadline : " + list.get(i).get("deadline") + "\n\n";
        }
        System.out.println(jawaban);
        return jawaban;
    }

}
